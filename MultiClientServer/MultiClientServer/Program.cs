﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;


namespace MultiClientServer
{
    class Program
    {
        static private int maxDistance = 2; // de max number of nodes, we gebruiken dit op een slimme manier om zo netwerkpartities af te vangen
        static public int MijnPoort; // Het poortnummer van deze 'node' - instance van Program
        static Object geschatteAfstandenLocker = new object(), routingTableLocker = new object(); // locker objects
        static public Dictionary<int, int> Distances = new Dictionary<int, int>();  // D table
        static public Dictionary<int, Connection> Buren = new Dictionary<int, Connection>(); // alle buren, connection access door Buren[poortnummer]
        static public Dictionary<int, Dictionary<int, int>> geschatteAfstanden = new Dictionary<int, Dictionary<int, int>>(); // de dictionary met geschatte afstanden vanaf elke buur
        // usage: geschatteAfstanden[buur][bestemming] = afstand
        static public Dictionary<int, int[]> routingTable = new Dictionary<int, int[]>(); // routingtable
        // usage: routingTable[bestemming] = int[]{ besteafstand, bestebuur }

        static void Main(string[] args)
        {
            // standard mode for testing
            if (args.Length == 0)
            {
                Console.Write("Op welke poort ben ik server? ");
                MijnPoort = int.Parse(Console.ReadLine());
                new Server(MijnPoort);
            }
            // met standaard invoer bij aanroep van MultiClientServer.exe
            else
            {
                MijnPoort = int.Parse(args[0]);
                new Server(MijnPoort);
                // we slapen even en proberen het later opnieuw, voor het geval we met iets verbinden wat nog niet bestaat
                Thread.Sleep(500);
                for (int i =1; i< args.Length; i++)
                {
                    bool tryagain = true;
                    while (tryagain) {
                        try {
                            tryagain = false;
                            Console.WriteLine("poging " + i);
                            if (int.Parse(args[i]) < MijnPoort) {
                                int nieuweBuur = int.Parse(args[i]);
                                VerbindNaarBuur(nieuweBuur);
                            }
                        }
                        catch {
                            Console.WriteLine("//er is iets fout gegaan");
                            Thread.Sleep(100);
                            tryagain = true;
                        }                  

                    }

                }
            }
            Console.Title = "NetChange poortnummer " + MijnPoort;
            Distances[MijnPoort] = 0;

            while (true) {
                int poort = 0; string bericht = ""; string[] delen = { "", "", "" };
                
                string input = Console.ReadLine();
                if (input == "R")
                {
                    ShowRoutingTable();
                }
                else {
                        // try to get portnumber and message
                        delen = input.Split(new char[] { ' ' }, 3);
                        poort = int.Parse(delen[1]);
                    if (input.StartsWith("B"))
                    {
                        // Stuur berichtje
                        try
                        {
                            bericht = delen[2];
                            SendMessage(delen[1] + " " + bericht);
                        }
                        catch
                        {
                            ShowError(poort); // poort onbekend
                        }
                    }
                    else if (poort == MijnPoort) throw new Exception(); // zal niet gebeuren volgens specificatie
                    //choose command
                    else if (input.StartsWith("C"))
                    {
                        //connect to poort
                        VerbindNaarBuur(poort);
                    }
                    else if (!Buren.ContainsKey(poort)) ShowError(poort);
                    else if (input.StartsWith("D"))
                    {
                        // disconnect with poort
                        Disconnect(poort);
                    }
                }               
            }
        }

        // doe een disconnect met een bepaalde poort
        private static void Disconnect(int poort)
        {
            try
            {
                Buren[poort].Write.WriteLine("D " + Program.MijnPoort);
                Buren[poort].InitiateDisconnect(poort); // do disconnect by closing Read and Write on Connection
                Buren.Remove(poort);
                UpdateGeschatteAfstanden(poort, poort, -1); // -1 is speciaal geval en vangen we af, dit wijst op een disconnect
            }
            catch
            {
                ShowError(poort); // poort onbekend
            }
        }

        // stuur een bericht naar een bepaalde poort
        public static void SendMessage(string message)
        {
                int bestemming = routingTable[int.Parse(message.Split()[0])][1];
                Buren[bestemming].Write.WriteLine("B " + message);
                Console.WriteLine("Bericht voor {0} doorgestuurd naar {1}", message.Split()[0], bestemming);
        }
        // poort is onbekend: laat een error message zien
        static void ShowError(int poort)
        {
            Console.WriteLine("Poort {0} is niet bekend", poort);
        }
        // leg een verbinding aan met deze buur
        static void VerbindNaarBuur(int buurPoort)
        {
            // Leg verbinding aan (als client)
            Buren.Add(buurPoort, new Connection(buurPoort));
            Distances[buurPoort] = 1;
            
            Console.WriteLine("Verbonden: {0}", buurPoort);
            UpdateBuren(buurPoort, 1);
            UpdateGeschatteAfstanden(buurPoort, buurPoort, 1);
            SendAllInfoToNeighbour(buurPoort);
            
        }

        // stuur alle info over routing naar een bepaalde buurman
        public static void SendAllInfoToNeighbour (int ontvanger)
        {
            lock (routingTableLocker) {
                foreach (KeyValuePair<int, int[]> combi in routingTable) {
                    int bestemming = combi.Key;
                    int buur = combi.Value[1];
                    int afstand = combi.Value[0];
                    Buren[ontvanger].Write.WriteLine(MyMessage(bestemming, afstand));
                }
            }
        }

        // update de geschatteafstandentabel obv een update van afstand vanaf een buur tot een bepaalde bestemming
        static public void UpdateGeschatteAfstanden (int buur, int bestemming, int afstand)
        {
            if (bestemming == MijnPoort) // dit weten we al natuurlijk
                return;
            lock (geschatteAfstandenLocker) {
                if (afstand == -1)
                { // er heeft een disconnect plaatsgevonden
                    foreach (KeyValuePair<int, Dictionary<int, int>> combi in geschatteAfstanden)
                    {
                        if (combi.Value.ContainsKey(buur)) combi.Value.Remove(buur);
                    }
                } else {
                    if (!geschatteAfstanden.ContainsKey(bestemming)) {
                        geschatteAfstanden.Add(bestemming, new Dictionary<int, int>());                       
                        Interlocked.Increment(ref maxDistance); // atomaire variant van maxDistance++;
                    }
                    if (!geschatteAfstanden[bestemming].ContainsKey(buur))
                        geschatteAfstanden[bestemming].Add(buur, afstand);
                    else {
                        if (afstand > maxDistance) {
                            afstand = maxDistance;
                        }
                        geschatteAfstanden[bestemming][buur] = afstand;
                    }
                }
            }
            RecomputeRoutingTable();    
        }

        // herbereken de gehele routing table
        private static void RecomputeRoutingTable()
        {
            lock (geschatteAfstandenLocker) {
                foreach (KeyValuePair<int, Dictionary<int, int>> bestemming in geschatteAfstanden) {
                    int bestebuur = 0;
                    int besteafstand = int.MaxValue;
                    foreach (KeyValuePair<int, int> buren in bestemming.Value) {
                        if (buren.Value < besteafstand) {
                            besteafstand = buren.Value;
                            bestebuur = buren.Key;
                        }
                    }
                    lock (routingTableLocker) {
                        if (besteafstand < maxDistance && bestebuur != 0) {
                            if (routingTable.ContainsKey(bestemming.Key)) {
                                if (routingTable[bestemming.Key][0] != besteafstand || routingTable[bestemming.Key][1]!= bestebuur) {
                                    // de afstand is gewijzigd
                                    UpdateBuren(bestemming.Key, besteafstand);       // update alle buren hiervan
                                    routingTable[bestemming.Key][0] = besteafstand;
                                    routingTable[bestemming.Key][1] = bestebuur;
                                    PrintNieuweAfstand(bestebuur, bestemming.Key, besteafstand);
                                }
                            } else {
                                routingTable.Add(bestemming.Key, new int[] { besteafstand, bestebuur });// er is een nieuwe bestemming bereikbaar geworden
                                UpdateBuren(bestemming.Key, besteafstand); // update alle buren hiervan
                                PrintNieuweAfstand(bestebuur, bestemming.Key, besteafstand);
                            }
                        } else {
                            if (routingTable.ContainsKey(bestemming.Key)) {
                                // een bestemming is onbereikbaar geworden
                                routingTable.Remove(bestemming.Key);
                                UpdateBuren(bestemming.Key, maxDistance + 1);
                                Console.WriteLine("onbereikbaar: " + bestemming.Key);
                            }
                        }
                    }
                }
            }
        }

        static void ShowRoutingTable()
        {
            Console.WriteLine(MijnPoort + " 0 local");
            lock (routingTableLocker){
                foreach (KeyValuePair<int,int[]> combi in routingTable)
                {
                    int bestemming = combi.Key;
                    int buur = combi.Value[1];
                    int afstand = combi.Value[0];
                    Console.WriteLine("{0} {1} {2}", bestemming, afstand, buur);
                }           
            }
        }

        static void PrintNieuweAfstand(int buur, int bestemming, int afstand) {
            Console.WriteLine("Afstand naar " + bestemming + " is nu " + afstand + " via " + buur);
        }

        // stuur een geheim bericht naar een buur
        public static string MyMessage(int buur, int afstand) {
            if (afstand == -1)
                return string.Format("/* {0} {1} {2}", MijnPoort, afstand, buur);
            else
                return string.Format("// {0} {1} {2}", MijnPoort, afstand, buur);
        }

        public static void UpdateBuren(int bestemming, int afstand) {
            string updatemessage = MyMessage(bestemming, afstand);
            foreach (KeyValuePair<int, Connection> combi in Buren) {
                int poort = combi.Key;
                Buren[poort].Write.WriteLine(updatemessage);
            }
        }

    }
}


