﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace MultiClientServer
{
    class Connection
    {
        public StreamReader Read;
        public StreamWriter Write;
        public TcpClient client;

        // Connection heeft 2 constructoren: deze constructor wordt gebruikt als wij CLIENT worden bij een andere SERVER
        public Connection(int port)
        {
            client = new TcpClient("localhost", port);
            Read = new StreamReader(client.GetStream());
            Write = new StreamWriter(client.GetStream());
            Write.AutoFlush = true;

            // De server kan niet zien van welke poort wij client zijn, dit moeten we apart laten weten
            Write.WriteLine("Poort: " + Program.MijnPoort);

            // Start het reader-loopje
            new Thread(ReaderThread).Start();
        }

        // Deze constructor wordt gebruikt als wij SERVER zijn en een CLIENT maakt met ons verbinding
        public Connection(StreamReader read, StreamWriter write)
        {
            Read = read; Write = write;

            // Start het reader-loopje
            new Thread(ReaderThread).Start();
        }

        // LET OP: Nadat er verbinding is gelegd, kun je vergeten wie er client/server is (en dat kun je aan het Connection-object dus ook niet zien!)

        // Deze loop leest wat er binnenkomt en print dit
        public void ReaderThread()
        {
            try
            {
                while (true) {
                    string line = Read.ReadLine();
                    string[] delen = line.Split();
                    int bestemming = int.Parse(delen[1]);
                    if (delen[0] == "D" && bestemming != Program.MijnPoort) {
                        Program.Buren.Remove(bestemming);
                        Program.UpdateGeschatteAfstanden(bestemming, bestemming, -1);
                        RecvieveDisconnect(bestemming);
                    } 
                    else if (line.StartsWith("//")) {
                        Program.UpdateGeschatteAfstanden(int.Parse(delen[1]), int.Parse(delen[3]), int.Parse(delen[2]) + 1);
                        //Console.WriteLine("bericht van {0} ontvangen, namelijk: {1}", delen[1], line);
                    }
                        
                    else if (line.StartsWith("B")) {
                        if (delen[1] == Program.MijnPoort.ToString())
                            Console.WriteLine(line.Split(new char[] { ' ' }, 3)[2]);
                        else {
                            Program.SendMessage(line.Split(new char[] { ' ' }, 2)[1]);
                        }

                    } else
                        Console.WriteLine(line);
                }
            }
            catch { } // Verbinding is kennelijk verbroken
        }

        public void InitiateDisconnect(int poort)
        {
            Read.Close();
            Write.Close();
            Console.WriteLine("verbroken: " + poort);
            if (client != null) {
                client.Close();
            }

        }

        public void RecvieveDisconnect(int poort)
        {
            Read.Close();
            Write.Close();
            Console.WriteLine("verbroken: " + poort);
            if (client != null) {
                client.Close();
            }
        }
    }
}
